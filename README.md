# OpenML dataset: chronic-kidney-disease

https://www.openml.org/d/43686

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset is originally from UCI Machine Learning Repository. The objective of the dataset is to diagnostically predict whether a patient is having chronic kidney disease or not, based on certain diagnostic measurements included in the dataset.
Content
The datasets consists of several medical predictor variables and one target variable, Class. Predictor variables includes Blood Pressure(Bp), Albumin(Al), etc.
Inspiration
Can you build a machine learning model to accurately predict whether or not the patients in the dataset have chronic kidney disease or not?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43686) of an [OpenML dataset](https://www.openml.org/d/43686). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43686/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43686/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43686/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

